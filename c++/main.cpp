/** @file main.cpp
 * Test for Space Applications candidates
 */

#include<dirent.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include "opencv2/features2d.hpp"

char getFolderSeparator()
{
  char sep = '/';
  #ifdef _WIN32
  sep = '\\';
  #endif
  return sep;
}

std::string getParentFolder(const std::string& s)
{
   size_t i = s.rfind(getFolderSeparator(), s.length());
   if (i != std::string::npos) {
      return(s.substr(0, i));
   }
   return("");
}

std::string createPath(const std::string& base_path, const std::string& folder)
{
  return std::string(base_path+getFolderSeparator()+folder);
}

void writeDetectionsToFile(const std::string &file_path, const std::string &detections)
{
  std::fstream file;
  file.open(createPath(file_path,"results.txt"), std::ios::out);
  file << detections<<std::endl;
  file.close();
}

std::vector<std::string> getImagesFromPath(const std::string &images_path)
{
  std::vector<std::string> files;
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (images_path.c_str())) != NULL)
  {
    while ((ent = readdir (dir)) != NULL)
    {
      if (std::string(ent->d_name).size() > 3)
      {
        files.push_back(createPath(images_path, ent->d_name));
      }
    }
    closedir (dir);
  }
  else
  {
    perror ("Directory does not exist");
  }
  return files;
}


void generateTemplate(cv::Mat &template_generated)
{
  /*
    Generate template for matching.
    Steps:
      1. load template image and the background image
      2. find contours and the approximated size of the template
      3. save the template
  */

  cv::Mat image_template;
  cv::Mat image_background;

  std::string file_path = __FILE__;
  std::string images_path = file_path.substr(0, file_path.rfind("\\"));
  std::string path_template = createPath(getParentFolder(getParentFolder(images_path)), "data/models_and_background/"
                                                                                        "can2.jpg");
  std::string path_background = createPath(getParentFolder(getParentFolder(images_path)), "data/models_and_background/"
                                                                                          "background.jpg");

  image_template = cv::imread(path_template);
  image_background = cv::imread(path_background);

  cv::Mat delta_img;

  cv::Mat back_grey, temp_grey;
  cv::cvtColor(image_template, back_grey, CV_BGR2GRAY);
  cv::cvtColor(image_background, temp_grey, CV_BGR2GRAY);

  cv::GaussianBlur(back_grey, back_grey, cv::Size(5, 5), 0);
  cv::GaussianBlur(temp_grey, temp_grey, cv::Size(5, 5), 0);

  cv::absdiff(temp_grey, back_grey, delta_img);
  cv::Mat img_thresh;
  cv::threshold(delta_img, img_thresh, 30, 255.0, CV_THRESH_BINARY);

  // cv::Mat structuringElement3x3 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
  // cv::Mat structuringElement5x5 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));
  cv::Mat structuringElement7x7 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7));
  // cv::Mat structuringElement9x9 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(9, 9));

  cv::erode(img_thresh, img_thresh, structuringElement7x7, cv::Point(-1, -1), 6);
  cv::dilate(img_thresh, img_thresh, structuringElement7x7, cv::Point(-1, -1), 6);

  std::vector<std::vector<cv::Point>> contours;

  cv::findContours(img_thresh, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);

  std::vector<cv::Point> contours_poly;
  cv::Rect boundRect;
  std::vector<cv::Point> main_contour;

  for (size_t i = 0; i < contours.size(); i++)
  {
    main_contour.insert(main_contour.end(), contours[i].begin(), contours[i].end());
  }

  approxPolyDP(main_contour, contours_poly, 3, true);
  boundRect = boundingRect(contours_poly);

  template_generated = image_template.clone();
  template_generated = template_generated(boundRect);
}

std::pair<cv::Point, cv::Point> detect(const cv::Mat &frame)
{
  /* Update this function with your detect logic */
  std::pair<cv::Point, cv::Point> bounding_box =
      std::make_pair(cv::Point(0, 0), cv::Point(100, 100));  // Initial value, to be updated

  // Add your code here

  /*
    Algorithm
    Steps:
      1. load template image only once
      2. Find ORB Features
      3. Filter ORB Features
      4. Find homography matrix and perspective transform
      5. Get bounding box
  */

  static cv::Mat template_image;
  static bool is_generated = false;

  if (!is_generated)
  {
    generateTemplate(template_image);
    is_generated = true;
  }

  // Detect
  cv::Ptr<cv::Feature2D> detector_ = cv::ORB::create(500, 1.200000048F, 8, 31, 0, 2, 0, 100, 20);

  std::vector<cv::KeyPoint> keypoints1 = std::vector<cv::KeyPoint>();
  cv::Mat descriptors1 = cv::Mat();
  std::vector<cv::KeyPoint> keypoints2 = std::vector<cv::KeyPoint>();
  cv::Mat descriptors2 = cv::Mat();

  detector_->detectAndCompute(template_image, cv::Mat(), keypoints1, descriptors1);
  detector_->detectAndCompute(frame, cv::Mat(), keypoints2, descriptors2);

  std::vector<cv::DMatch> matches;
  cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
  std::vector<std::vector<cv::DMatch>> knn_matches;
  if (descriptors1.type() != CV_32F)
  {
    descriptors1.convertTo(descriptors1, CV_32F);
  }

  if (descriptors2.type() != CV_32F)
  {
    descriptors2.convertTo(descriptors2, CV_32F);
  }
  matcher->knnMatch(descriptors1, descriptors2, knn_matches, 2);
  // Filter matches using the Lowe's ratio test

  const float ratio_thresh = 0.9f;
  std::vector<cv::DMatch> good_matches;
  for (size_t i = 0; i < knn_matches.size(); i++)
  {
    if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
    {
      good_matches.push_back(knn_matches[i][0]);
    }
  }

  std::vector<cv::Point2f> obj;
  std::vector<cv::Point2f> scene;
  for (size_t i = 0; i < good_matches.size(); i++)
  {
    //-- Get the keypoints from the good matches
    obj.push_back(keypoints1[good_matches[i].queryIdx].pt);
    scene.push_back(keypoints2[good_matches[i].trainIdx].pt);
  }
  cv::Mat H = findHomography(obj, scene, cv::RANSAC);

  std::vector<cv::Point2f> obj_corners(4);
  obj_corners[0] = cv::Point2f(0, 0);
  obj_corners[1] = cv::Point2f(template_image.cols, 0);
  obj_corners[2] = cv::Point2f(template_image.cols, template_image.rows);
  obj_corners[3] = cv::Point2f(0, template_image.rows);

  std::vector<cv::Point2f> scene_corners(4);
  perspectiveTransform(obj_corners, scene_corners, H);
  bounding_box.first = scene_corners[0];
  bounding_box.second = scene_corners[2];

  return bounding_box;
}

int main(int argc, char** argv)
{
  /* Do not modify this function */
  std::string detections = "";
  std::string file_path = __FILE__;
  std::string images_path = file_path.substr(0, file_path.rfind("\\"));
  images_path = createPath(getParentFolder(getParentFolder(images_path)), "data/test_images");
  auto files = getImagesFromPath(images_path);

  for (auto file : files)
  {
    cv::Mat frame = cv::imread(file, CV_LOAD_IMAGE_COLOR);
    if (frame.empty() == false)
    {
      auto bounding_box = detect(frame);
      detections +=  file +" "+std::to_string(bounding_box.first.x) \
                          +" "+std::to_string(bounding_box.first.y) \
                          +" "+std::to_string(bounding_box.second.x) \
                          +" "+std::to_string(bounding_box.second.y) +"\n";

      cv::rectangle(frame, bounding_box.first, bounding_box.second, cv::Scalar(255, 0, 0), 3);
      cv::imshow("Detection", frame);
      if (cv::waitKey(0) == 'q')
      {
        break;
      }
    }
  }

  writeDetectionsToFile(getParentFolder(file_path), detections);
  return 0;
}
